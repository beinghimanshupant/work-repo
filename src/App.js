import React, { Component } from "react";
import PropTypes from "prop-types";
import { Route,Router } from "react-router-dom";
import LoginContainer from "../src/Screens/Login/LoginContainer";

class App extends Component {
  render() {
    const { history } = this.props;

    return (
      <Router history={history}>
          <Route
          exact
            path='/'
            component={LoginContainer}
          />
      </Router>
    );
  }
}

App.propTypes = {
  history: PropTypes.object.isRequired
};

export default App;