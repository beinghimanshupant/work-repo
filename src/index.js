import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import createHistory from "history/createBrowserHistory";
import rootReducer from "../src/rootReducer"
// import registerServiceWorker from "./registerServiceWorker";

import App from "./App";
import { createStore } from "../src/store";

const history = createHistory();

const store = createStore(rootReducer, history);

ReactDOM.render(
  <Provider store={store}>
    <App history={history} />
  </Provider>,
  document.getElementById("root")
);