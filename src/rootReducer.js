import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import { reducer as formReducer } from 'redux-form'
import  {reducers as loginReducer} from "../src/Screens/Login/reducers"



export default combineReducers({
  routing: routerReducer,
  form : formReducer,
  loginReducer
});