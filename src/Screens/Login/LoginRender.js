import React from "react"
import { Field, reduxForm } from 'redux-form'

const LoginRender = (props) => {
    const { handleSubmit, pristine, submitting, checkUserLogin, error } = props
    return (<div className="container">
        <div className="row">
            <div className="col-md-4 col-md-offset-4">
            Login Here
                <form onSubmit={handleSubmit((value) => { checkUserLogin(value) })}>

                    <div className="panel-body">
                        <div className="form-group">
                            <Field
                                name="userName"
                                component="input"
                                type="text"
                                placeholder="UserName"
                                className="form-control"
                                label="UserName"
                            />

                        </div>
                        <div className="form-group">
                            <Field
                                name="password"
                                component="input"
                                type="password"
                                placeholder="Password"
                                className="form-control"
                                label="Password"
                            />

                        </div>
                    </div>
                    {error && <strong>{error}</strong>}
                    <div>
                        <button type="submit" disabled={pristine || submitting} className="btn btn-lg btn-success btn-block">
                            LogIn
          </button>
                    </div>
                </form>
            </div>
        </div>
    </div>)

}

export default reduxForm({
    form: 'loginForm'
})(LoginRender)