import React from "react"
import LoginRender from "./LoginRender"
import { connect } from "react-redux";
import { SubmissionError } from 'redux-form'
import {getData} from "./actions"



class LoginContainer extends React.Component{
    constructor(props){
        super(props)
        this.state={isAuthenticatedUser:false}
    }
    
    componentDidMount(){
this.props.getData()
     
    }
    checkUserLogin =(values)=>{
        if (!['admin', 'omkar'].includes(values.userName)) {
            throw new SubmissionError({
              username: 'User does not exist',
              _error: 'Login failed!'
            })
          } else if (values.password !== '123456') {
            throw new SubmissionError({
              password: 'Wrong password',
              _error: 'Login failed!'
            })
          }
          else{
              this.setState({isAuthenticatedUser:true})
          }

    }

    render()
    { const {isAuthenticatedUser} = this.state
        return(
            <div>
              {  !isAuthenticatedUser ?
              <LoginRender checkUserLogin={this.checkUserLogin}/>
              : null
              }
                
            </div>

        )
    }
}
    const mapStateToProps = state => ({

    })
    const mapDispatchToProps = {
        getData
    }
    
    export default connect(
        mapStateToProps,
        mapDispatchToProps
    )(LoginContainer);