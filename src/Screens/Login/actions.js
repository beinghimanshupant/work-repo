import fetch from 'cross-fetch'
export const getData=()=>{
    return dispatch=>{ fetch(`http://dummy.restapiexample.com/api/v1/employees`)
    .then(response=>response.json())
    .then( data =>{
        dispatch({
            type:"GET_DATA",
            payload: {data :data}
        })
    })
}
}